#include<stdio.h>
#include <stdlib.h>
#include <stdarg.h>

int main()
{
	int tamanio, i;
	int *pVector1;
	int *pVector2;
	
	
	printf("Ingrese el tamaño de los vectore 1 y 2\n");
	scanf("%d", &tamanio);
	int vector1[tamanio];
	int vector2[tamanio];
	
	//llenado del vector 1
	printf("Ingrese los valores del vector 1\n");
	for (i = 0; i < tamanio; i++)
	{
		
		scanf("%d", &vector1[i]);
		
	}
	
	//llenado del vector 2
	printf("Ingrese los valores del vector 1\n");
	for (i = 0; i < tamanio; i++)
	{
		scanf("%d", &vector2[i]);
		
	}
	
	pVector1=vector1;
	pVector2=vector2;
	
	//se hace el intercambio de vetores
	int *aux;
	aux=pVector1;
	pVector1=pVector2;
	pVector1=aux;
	
	//Se imprimen los vectores ya intercambiados
	printf("Primer vector con valores intercambiados \n");
	for (i = 0; i < tamanio; i++)
	{
		printf("%d\t", pVector1[i]);
	}
	
	printf("Segundo vector con valores intercambiados \n");
	for (i = 0; i < tamanio; i++)
	{
		printf("%d\t", pVector2[i]);
	}
	

}
