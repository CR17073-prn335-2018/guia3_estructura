#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

int main()
{
	int tamanio;
	int i;
	int *pVec;
	
	// se pide el tamaño del vector
	printf("Ingrese el tamaño del vector\n");
	scanf("%d", &tamanio);

	int vector[tamanio];

//Se llena el vector
	printf("Ingrese datos al vector\n");
	for (i = 0; i < tamanio; i++)
{
	scanf("%d", &vector[i]);
}
	 pVec=vector;
	 
	 //Se muestra el vector por medio de punteros
	 printf("Vector utilizando los punteros\n");
	 for (i = 0; i < tamanio; i++)
{
	printf("%d\t", pVec[i]);
}
	 
	 //Se imprime el vector en reversa
	 printf("Vector en reversa");
	 for (i = tamanio-1; 0 <= i ; i--)
	 {
		 printf("%d", vector[i]);
	 }
	 
	 
return 0;
}
