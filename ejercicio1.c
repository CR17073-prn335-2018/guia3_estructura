#include<stdio.h>
#include <stdlib.h>
#include <stdarg.h>


int main()
{ 
	int menu;
	int primero, segundo;
	int *p, *s;
	int suma, resta;
	
	
	//Bucle do while para poder permanecer en el menu
	do{
		//Vista del menu
	printf("1.Ingresar dos numeros enteros \n2.Calcula suma con punteros \n3.Calcula resta con punteros \n4.Imprimir direccion de memoria de cada variable.\n5.salir\n");
	scanf("%d", &menu);
	
	
	
	switch (menu)
	{
		
		//Pedimos los valores al usuario
		case 1 :
		printf("Ingrese dos numeros Enteros:\n");
		printf("Primer numero:");
		scanf("%d", &primero);
		
		printf("Segundo numero");
		scanf("%d", &segundo);
		p=&primero;
		s=&segundo;
			
			break;
			
		case 2:
		suma= *p + *s;
		
		//Hacemos la suma utilizando punteros
		printf("La suma de los punteros es %d", suma);
		printf("\n");
		
		break;
		
		case 3:
		p=&primero;
		s=&segundo;
		//Hacemos la resta utilizando punteros
		resta=*p - *s ;
		printf("La resta de los punteros es %d", resta );
		printf("\n");
		
		break;
		
		
		case 4:
		p=&primero;
		s=&segundo;
		printf("La direccion de memoria del primer numero es:");
		printf("%d", &primero);
		
		printf("La direccion de memoria del segundo numero es ");
		printf("%d", &segundo);
		printf("\n");
		break;
		}
	
	
	}while(menu !=5 );
	
		
	
	return 0;
	}
